from flask import Flask
import toml
from .models import db
from . import api, frontend

app = Flask(__name__)
with app.open_instance_resource("config.toml", "r") as config_file:
    config = toml.load(config_file)
app.config.update(config["flask-config"])
db.init_app(app)

app.register_blueprint(frontend.bp)
app.register_blueprint(api.bp, url_prefix="/api")
