from . import db
from .enums import *


class MediaItem(db.Model):
    __tablename__ = "media_item"
    id = db.Column(db.Integer, primary_key=True)
    media_type = db.Column(db.Enum(MediaType), nullable=False)
    versions = db.relationship("MediaVersion", backref="media_item")  # fills media_item for MediaVersion
    name = db.Column(db.String(255), nullable=True)
    description = db.Column(db.String, nullable=True)
    subject = db.Column(db.String(255), nullable=True)
    # collections: filled by backref
#    photographer_id = db.Column(db.Integer, db.ForeignKey("photographer.id"), nullable=True)

    @property
    def original(self):
        x = [x for x in self.versions if x.type == VersionType.original]
        if x:
            return x[0]
        else:
            return None

    @property
    def type(self):
        return self.original.type

    # def __repr__(self):
    #     return f"<Media: {self.type.name} {self.id}>"


class MediaVersion(db.Model):
    __tablename__ = "media_version"
    id = db.Column(db.Integer, primary_key=True)
    media_item_id = db.Column(db.Integer, db.ForeignKey("media_item.id"), nullable=False)
    #  media_item: filled by backref
    version_type = db.Column(db.Enum(VersionType), nullable=False)
    media_type = db.Column(db.Enum(MediaType), nullable=False)
    size_x = db.Column(db.Integer, nullable=True)
    size_y = db.Column(db.Integer, nullable=True)
    location_type = db.Column(db.Enum(LocationType), nullable=False)
    location = db.Column(db.String, nullable=False)

    # Gets the media as a binary file handle
    def fetch_file(self):
        pass

    # Gets the media as bytes
    def fetch_bytes(self):
        pass

    # def __repr__(self):
    #     return f"<MediaVersion: {self.id} of {self.media_id}>"
