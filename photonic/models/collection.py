from . import db
from .enums import *

# Many-many association table between MediaItem and Collection
media_item_collection_assoc = db.Table(
    "media_item_collection_assoc", db.Model.metadata,
    db.Column("collection_id", db.Integer, db.ForeignKey("collection.id")),
    db.Column("media_item_id", db.Integer, db.ForeignKey("media_item.id"))
)


class Collection(db.Model):
    __tablename__ = "collection"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=True)
    description = db.Column(db.String, nullable=True)
#    collection_type = db.Column(db.Enum(CollectionType), nullable=False)
    special = db.Column(db.String(255), nullable=True)
    parent_id = db.Column(db.Integer, db.ForeignKey("collection.id"), nullable=True)
    # parent: filled by backref
    children = db.relationship("Collection", backref=db.backref("parent", remote_side=id))
    media_items = db.relationship("MediaItem", secondary=media_item_collection_assoc, backref="collections")