from enum import Enum


class MediaType(Enum):
    photo = 1
    video = 2


class VersionType(Enum):
    original = 1
    resized = 2
    thumbnail = 3
    snapshot = 4


class LocationType(Enum):
    local = 1  # location is relative path to file
    url = 2  # location is the url to file
    google_photos = 3  # location is the mediaItemId


# class CollectionType(Enum):
#     normal = 1  # Regular photo collection
#     special = 2  # Special collection, such as a group of photos for a pano
